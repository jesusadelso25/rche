import { IonApp, IonRouterOutlet, IonSplitPane, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { Route } from 'react-router-dom';
/**contenedores de class objecto */
import ActivitiesContextProvider from './components/Actividades/ActivitiesContextProvider';
import EquiposContextProvider from './components/EquiposElectr/EquiposContextProvider';
import EquiposCasaContextProvider from './components/Estadisticas/EstadistcasContextProvider';
import SuplidorasContextProvider from './components/SuplEnergia/SuplidorContProvider';
import UsersContextProvider from './components/Usuario/UsuarioContexProvider';
/*importacion de lista de navegador */
import Menu from './components/menu/Menu';
import Inicio from './pages/Home/Home';
import Nosotros from './pages/nosotro/Nosotros';
import ListCasa from './pages/Estadistica/ListCasa';
import AddCasa from './pages/Estadistica/AddCasa'
import ListSimulador from './pages/Simulador/ListSimulador';
import Simular from './pages/Simulador/SimularCalculo';
import Long from './pages/UsuarioVRA/VR';
import AddActivid from './pages/Activid/AddActivid';
import ListaActivid from './pages/Activid/ListActivid';
import ActUser from './pages/UsuarioVRA/UpdateUSuario';
import Manual from './pages/nosotro/Manual';

import '@ionic/react/css/core.css';
/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
setupIonicReact();
const App: React.FC = () => {

  return (
    <IonApp>
      <IonReactRouter>
        <IonSplitPane contentId="main">
          <Menu />
            <IonRouterOutlet id="main">
           
            <SuplidorasContextProvider><EquiposContextProvider><ActivitiesContextProvider><EquiposCasaContextProvider><UsersContextProvider>
            <Route path="/" exact={true}>
              <Long />
            </Route>
            <Route path="/Login/" exact={true}>
              <Long/>
            </Route>
            <Route path="/Inicio" exact={true}>
              <Inicio/>
            </Route>
            <Route path="/ActUser" exact={true}>
            <ActUser/>
            </Route>
            <Route path="/Nosotros" exact={true}>
              <Nosotros/>
            </Route>
            <Route path="/Manual" exact={true}>
              <Manual/>
            </Route>
            <Route path="/Estaditica" exact={true}>
            <ListCasa/>
            </Route>
            <Route path="/Simulador" exact={true}>
            <ListSimulador/>
            </Route>
            <Route path="/Actividades" exact={true}>
            <ListaActivid/>
            </Route>
            <Route path="/addactividades" exact={true}>
            <AddActivid/>
            </Route>
            <Route path="/Simular/:id" exact={true}>
            <Simular/>
            </Route>
            <Route path="/AddEsta/:id" exact={true}>
            <AddCasa/>
            </Route>
            </UsersContextProvider></EquiposCasaContextProvider></ActivitiesContextProvider></EquiposContextProvider></SuplidorasContextProvider>
          </IonRouterOutlet>
          
        </IonSplitPane>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
