import { calculator, returnUpBack, cut, arrowBack } from 'ionicons/icons';
import EquiposContext from '../../components/EquiposElectr/EquipoConst';
import suplidorContext from '../../components/SuplEnergia/SuplodorCont';
import { useParams } from "react-router";
import { IonItem, IonSelectOption, IonSelect, IonTitle, IonButtons, IonToolbar, IonHeader, IonIcon, IonCardContent, IonButton, IonContent, IonLabel, IonPage, IonGrid, IonRow, IonCol, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';
import { useState, useContext, useEffect } from 'react';
import '../css/Style.css'
import swal from 'sweetalert'
const Simular: React.FC = () => {

    type QuizParams = {
        id: string;
    };

    const { id } = useParams<QuizParams>()
    /*Alerta de msg */
    const alert = (titulo: string, text: string, icon: string) => {
        swal({
            title: titulo,
            text: text,
            icon: icon,
        });
    }
    const equiposCtxt = useContext(EquiposContext);
    const suplidorasCtxt = useContext(suplidorContext);

    const [selecHora, getselecHora] = useState<number>();
    const [idsuplidora, getidsuplidora] = useState('');
    var [Preciosuplidor, setPreciosuplidor] = useState('');
    var [calculoDeIMC, setCalculoDeIMC] = useState('0');

    const [AxCalculo, setAxCalculo] = useState<number>();
    const [AxCalculov, setAxCalculov] = useState('');
    const [AxCalculoid, setAxCalculoid] = useState('');


    const [activarboton, setactivarboton] = useState(false);
    const [activarboton2, setactivarboton2] = useState(false);
    const pemaddactiidades = () => {
        if (activarboton != null) {
            setactivarboton(true);
        }
    }



    const buscarsuplidora = (id: string) => {
        {
            suplidorasCtxt.suplidoras.map(Suplidora => {
                {
                    if (id === Suplidora.id) {
                        setPreciosuplidor(Suplidora.precio);
                    }
                }
            })
        }
    }

    const calcularIMC = (voltaje: string, id: string) => {
        setAxCalculo(selecHora);
        setAxCalculov(voltaje);
        setAxCalculoid(id);
        if (Number(selecHora) > 0) {
            if (id === '') {
                alert('Error :(', 'Debe Selecionar una suplidora para determinar el precio', 'warning');
            } else {
                buscarsuplidora(id)
                var imc = ((Number(selecHora) * Number(voltaje)) * Number(Preciosuplidor));
                setCalculoDeIMC('' + imc);
                setactivarboton2(true);
            }
        } else {
            alert('Error :(', 'Si no tienes horas  en la semana, por ley es resultado es cero', 'warning');
        }
    }
    useEffect(() => {
        if (Number(calculoDeIMC) > 0) {
            if (AxCalculo === selecHora) { } else {
                setactivarboton(true);
                calcularIMC(AxCalculov, AxCalculoid);
            }
            if (AxCalculoid === idsuplidora) { } else {
                setactivarboton(true);
                calcularIMC(AxCalculov, AxCalculoid);
            }
        }
        if (Number(selecHora) > 0) {
            if (Number(idsuplidora) > 0) {
                buscarsuplidora(idsuplidora);
                pemaddactiidades();
            }
        }

    });
    function Limpiarcalculo() {
        setactivarboton(false);
        setactivarboton2(false);
        getselecHora(0);
        getidsuplidora('')
        setCalculoDeIMC('0');
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonButton routerLink={"/Simulador"}><IonIcon icon={arrowBack} /></IonButton>
                    </IonButtons>
                    <div style={{ textAlign: "center", position: "relative" }}>
                        <IonLabel>Simulador</IonLabel><br></br>
                    </div>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen style={{ position: 'relative', width: '100%', height: '100%' }}>
                <div className="ion-text-center" >
                    <div> {equiposCtxt.equipos.filter(Equipos => Equipos.id.includes(id)).map((Equipos,index) =>
                        <IonGrid className="ion-grid1" >
                            <IonRow key={index}>
                                <IonCol className="ion-text-center"  >
                                    <IonCard>
                                        <IonTitle>Equipo:{Equipos.Equipo}</IonTitle>
                                        <img className="img" src={Equipos.imageUrl} />
                                        <IonCardHeader>
                                            <IonCardTitle>Marca:{Equipos.Marca}</IonCardTitle>
                                            <IonCardTitle >Consumo encedido:{Equipos.voltaje}Wh    </IonCardTitle>
                                        </IonCardHeader>
                                        <IonCardContent>
                                            <IonItem>
                                                <IonLabel>Horas</IonLabel>
                                                <IonSelect value={selecHora} placeholder="Select horas" onIonChange={(e: any) => getselecHora(e.detail.value)}>
                                                    {Array(100).fill(1).map((_, i) =>
                                                        <IonSelectOption key={i} value={i}>{i} Horas</IonSelectOption>
                                                    )}
                                                </IonSelect>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Suplidor</IonLabel>
                                                <IonSelect value={idsuplidora} placeholder="Select suplidora" onIonChange={(e: any) => getidsuplidora(e.detail.value)}>
                                                    {suplidorasCtxt.suplidoras.map(Suplidoras =>
                                                        <div>
                                                            <IonSelectOption key={Suplidoras.id} value={Suplidoras.id}>{Suplidoras.suplidora}.. {Suplidoras.precio}$</IonSelectOption>
                                                        </div>
                                                    )}
                                                </IonSelect>
                                            </IonItem>
                                            <IonCardTitle><h3>Resultado RD${calculoDeIMC}</h3></IonCardTitle>
                                            <IonButton fill="outline" shape="round" color="--ion-color-primary" onClick={() => calcularIMC(Equipos.voltaje, idsuplidora)} disabled={!activarboton}> <IonIcon icon={calculator} />Calcular </IonButton>
                                            <IonButton fill="outline" shape="round" color="danger" onClick={Limpiarcalculo} disabled={!activarboton2} >  <IonIcon icon={cut} />Limpiar </IonButton>
                                        </IonCardContent>
                                    </IonCard>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                    )}
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Simular