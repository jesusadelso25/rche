
import React, { useContext, useState } from 'react'
import { arrowForward, home } from 'ionicons/icons';
import EquiposContext from '../../components/EquiposElectr/EquipoConst';
import '../css/Style.css'
import { IonSegment, IonIcon, IonSearchbar, IonSegmentButton, IonButton, IonButtons, IonContent, IonCardContent, IonHeader, IonLabel, IonMenuButton, IonPage, IonTitle, IonToolbar, IonAvatar, IonGrid, IonRow, IonCol, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';

const Equipos: React.FC = () => {
    const equiposCtxt = useContext(EquiposContext);
    const [busqueda, setbussqueda] = useState("");
    const [searchText, setsearchText] = useState("");
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Lista de Equipos</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen style={{ position: 'relative', width: '100%', height: '100%' }}>
                <IonRow>
                    <IonCol className='ion-text-center'>
                        <IonSearchbar value={searchText} placeholder='Buscar por Nombre ' onIonChange={e => setsearchText(e.detail.value!)}></IonSearchbar>
                        <IonSegment value={busqueda} placeholder="Select" onIonChange={(e: any) => setbussqueda(e.detail.value)} >
                            <IonSegmentButton value=''>
                                <IonLabel>Todos</IonLabel>
                            </IonSegmentButton>
                            <IonSegmentButton value='Alto'>
                                <IonLabel >Alto</IonLabel>
                            </IonSegmentButton>
                            <IonSegmentButton value='Medio'>
                                <IonLabel>Medio </IonLabel>
                            </IonSegmentButton>
                            <IonSegmentButton value='Bajo'>
                                <IonLabel>Bajo</IonLabel>
                            </IonSegmentButton>
                        </IonSegment>
                    </IonCol>
                </IonRow>
                <IonTitle className="ion-text-center">Lista de Equipos del hogar</IonTitle>
                {
                    searchText ?
                        <div>
                            <IonGrid className="ion-grid" >
                                {equiposCtxt.equipos.filter(Equipos => Equipos.Equipo.toUpperCase().includes(searchText.toUpperCase())).map((Equipos,index) =>
                                    <IonRow key={index}>
                                        <IonCol className="ion-text-center">
                                            <IonCard>
                                            <h5 className='Titulo'>Equipo:{Equipos.Equipo}</h5>
                                                <img className="img" src={Equipos.imageUrl} />
                                                <IonCardHeader>
                                                    <IonCardTitle>Marca:{Equipos.Marca}</IonCardTitle>
                                                    <IonCardTitle>Consumo encedido: {Equipos.voltaje} Wh </IonCardTitle>
                                                </IonCardHeader>
                                                <IonButton fill='outline' color="--ion-color-primary" routerLink={"AddEsta/" + Equipos.id}>  <IonIcon icon={home} /> Agregar </IonButton>
                                                <IonButton fill='outline' color="--ion-color-primary" routerLink={"Simular/" + Equipos.id} > <IonIcon icon={arrowForward} />Select </IonButton>

                                                <IonCardContent>
                                                </IonCardContent>
                                            </IonCard>
                                        </IonCol>
                                    </IonRow>)}
                            </IonGrid>

                        </div> :
                        <div>
                            {busqueda ?
                                <div>
                                    <IonGrid className="ion-grid" >
                                        {equiposCtxt.equipos.filter(Equipos => Equipos.nivelvoltaje.includes(busqueda)).map((Equipos,index) =>
                                            <IonRow key={index}>
                                                <IonCol className="ion-text-center">
                                                    <IonCard>
                                                    <h5 className='Titulo'>Equipo:{Equipos.Equipo}</h5>
                                                        <img className="img" src={Equipos.imageUrl} />
                                                        <IonCardHeader>
                                                            <IonCardTitle>Marca:{Equipos.Marca}</IonCardTitle>
                                                            <IonCardTitle>Consumo encedido: {Equipos.voltaje} Wh </IonCardTitle>
                                                        </IonCardHeader>
                                                        <IonButton fill='outline' color="--ion-color-primary" routerLink={"AddEsta/" + Equipos.id}>  <IonIcon icon={home} /> Agregar </IonButton>
                                                        <IonButton fill='outline' color="--ion-color-primary" routerLink={"Simular/" + Equipos.id} > <IonIcon icon={arrowForward} />Select </IonButton>

                                                        <IonCardContent>
                                                        </IonCardContent>
                                                    </IonCard>
                                                </IonCol>
                                            </IonRow>)}
                                    </IonGrid>
                                </div>
                                :
                                <div>
                                    <IonGrid className="ion-grid" >
                                        {equiposCtxt.equipos.map((Equipos,index) =>
                                            <IonRow key={index}>
                                                <IonCol className="ion-text-center">
                                                    <IonCard>
                                                    <h5 className='Titulo'>Equipo:{Equipos.Equipo}</h5>
                                                        <img className="img" src={Equipos.imageUrl} />
                                                        <IonCardHeader>
                                                            <IonCardTitle>Marca:{Equipos.Marca}</IonCardTitle>
                                                            <IonCardTitle>Consumo encedido: {Equipos.voltaje} Wh </IonCardTitle>
                                                        </IonCardHeader>
                                                        <IonButton fill='outline' shape="round" color="--ion-color-primary" routerLink={"AddEsta/" + Equipos.id}>  <IonIcon icon={home} /> Agregar </IonButton>
                                                        <IonButton fill='outline' shape="round" color="--ion-color-primary" routerLink={"Simular/" + Equipos.id} > <IonIcon icon={arrowForward} />Select </IonButton>
                                                        <IonCardContent>
                                                        </IonCardContent>
                                                    </IonCard>
                                                </IonCol>
                                            </IonRow>)}
                                    </IonGrid>
                                </div>
                            }
                        </div>}
            </IonContent>
        </IonPage>
    )
}

export default Equipos