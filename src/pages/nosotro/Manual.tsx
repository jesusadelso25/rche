import React,{ useState} from 'react';
import { IonButtons, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './styles.css'
const Manual = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Manual</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonHeader collapse="condense">
                <IonToolbar>
                    <IonTitle size="large">Nosotros</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <div style={{ position: 'relative', width: '100%', height: '100%', textAlign: 'center' }}>
                    <br />
                    <br />
                    <br />
                    <br />
                    <a href='https://docs.google.com/document/d/1JxE2PeG2Gm9VU7nSWJSfyjGSGAcEOzbV/edit?usp=sharing&ouid=109308910395390655449&rtpof=true&sd=true' id="download"> IR a PDF
                    </a>
                    <label >
                        <br />
                        Aqui se mostrara un pdf con la indicaciones de cada pagina,por el momento se mostrara en otra pagina
                    </label>

                </div>

            </IonContent>
        </IonPage>
    )
}

export default Manual