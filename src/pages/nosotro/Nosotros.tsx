import React from 'react';
import log from '../../components/asset/image/icon.png';
import { IonButtons, IonText, IonSlides, IonSlide, IonContent, IonHeader, IonMenuButton, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import './styles.css'
const Nosotros = () => {
  const slideOpts = {
    initialSlide: 0,
    speed: 100
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonTitle>Nosotros</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonHeader collapse="condense">
        <IonToolbar>
          <IonTitle size="large">Nosotros</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonSlides pager={true} options={slideOpts} className="Ionslides" style={{ position: 'relative', width: '100%', height: '100%' }}>
          <IonSlide>
            <div className='center'>
              <img src={log}></img>
              <h1>¿Que es RCHE?</h1><br></br>

              <h4 className="textSl">
                Remote Control Home Energy es una aplicación móvil, capaz de realizar un cálculo del consumo de energía eléctrica en el hogar.
                Este le permite al usuario crear una lista de equipos del hogar para dar un estimado del consumo. También cuenta con un simulador
                de equipos para simular el consumo del equipo. Informaciones a mostrar reportes de estimado de consumo en modo de gráficos por
                periodos de tiempos, recomendaciones para un uso racional de energía en el hogar</h4>
              <IonText color="success"></IonText>
            </div>
          </IonSlide>
          <IonSlide>
            <div className='center'>
              <img src={log}></img>
              <h1>Objectivos</h1><br></br>
              <h4 className="textSl">
                <br></br>● Controlar de manera eficiente el consumo.
                <br></br>● Formar hábitos sobre el consumo de energía.
                <br></br>● Reducir el impacto sobre el cambio climático.
              </h4>
            </div>

          </IonSlide>
          <IonSlide >
            <div className='center'>
              <img src={log}></img>
              <h1>Desarrollado por:</h1><br></br>
              <h4 className="textSl">
                <br></br >Adelso de Jesus Muñoz Taveras.
                <br></br> Elvin Francier Gabriel de Jesus.
                <br></br> Orquidia Valdez Lajara.
              </h4>
              <br></br>
              <br></br>
              <a>All rights reserved</a>
            </div>
          </IonSlide>
        </IonSlides>
      </IonContent>
    </IonPage>
  )
}

export default Nosotros