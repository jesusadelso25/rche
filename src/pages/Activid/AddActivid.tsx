import React, { useContext, useState } from 'react';
import {
    IonPage,
    IonHeader,
    IonToolbar,
    IonTitle,
    IonContent,
    IonGrid,
    IonButtons,
    IonMenuButton,
    IonRow,
    IonCol,
    IonSegment,
    IonSegmentButton,
    IonLabel,
    IonItem,
    IonInput,
    IonButton,
} from '@ionic/react';
import ActivitiesContext, { ActivityType } from '../../components/Actividades/ActividadesConst';
import swal from 'sweetalert'

const AddActivity: React.FC = () => {
    const activitiesCtxt = useContext(ActivitiesContext);
    const [activarboton, setactivarboton] = useState(false);
    const alert = (titulo: string, text: string, icon: string) => {
        swal({
            title: titulo,
            text: text,
            icon: icon,
        });
    }
    const [title, gettitle] = useState('');
    const [description, getdescription] = useState('');
    const [timeion, gettimeion] = useState('');
    const [activityType, getactivityType] = useState('');

    const pemaddactiidades = () => {
        if (activityType != null) {
            setactivarboton(true);
        } else {
            setactivarboton(true);
        }
    }
    const addActivity = () => {
        const sugerencia = false;
        const imageUrl = "";
        if (title && description && timeion && activityType) {
            activitiesCtxt.addActivitynube(title, description, sugerencia, timeion, activityType as ActivityType, imageUrl);
            alert('Bien :) ', 'se guardo correctamente!', 'success');
            gettitle('');
            getdescription('');
            gettimeion('');
        } else {
            alert('Error :(', 'Favor de llenar los campos correpondiente', 'warning');
        }
    };

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Actividades</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen style={{ position: 'relative', width: '100%', height: '100%' }}>
                <IonGrid>
                    <IonRow>
                        <IonCol className='center'>
                            <IonSegment onIonChange={(e: any) => getactivityType(e.detail.value)} onClick={pemaddactiidades}>
                                <IonSegmentButton value='apagar'>
                                    <IonLabel>apagar</IonLabel>
                                </IonSegmentButton>
                                <IonSegmentButton value='cambiar'>
                                    <IonLabel>Cambiar </IonLabel>
                                </IonSegmentButton>
                                <IonSegmentButton value='limpiar'>
                                    <IonLabel>Limpiar</IonLabel>
                                </IonSegmentButton>
                            </IonSegment>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <IonItem>
                                <IonLabel position='floating'>
                                    Titulo de activida
                                </IonLabel>
                                <IonInput value={title} onIonChange={(e: any) => gettitle(e.detail.value)} type='text' disabled={!activarboton}></IonInput>
                            </IonItem>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <IonItem>
                                <IonLabel position='floating'>
                                    Descripción de actividad
                                </IonLabel>
                                <IonInput value={description} onIonChange={(e: any) => getdescription(e.detail.value)} type='text' disabled={!activarboton}></IonInput>
                            </IonItem>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            <IonItem>
                                <IonLabel position='floating'>
                                    horas
                                </IonLabel>
                                <IonInput value={timeion} onIonChange={(e: any) => gettimeion(e.detail.value)} type='time' disabled={!activarboton}></IonInput>
                            </IonItem>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol className='ion-margin-top'>
                            <IonButton expand='block' fill='outline' shape="round" onClick={addActivity} disabled={!activarboton}>
                                Agregar actividad
                            </IonButton>
                        </IonCol>
                        <IonCol className='ion-margin-top'>
                            <IonButton expand='block' fill='outline' shape="round" routerLink={"Actividades"}>
                                Lista de actividades
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default AddActivity;
