import React, { useContext, useState } from 'react';
import { IonPage, IonHeader, IonBadge, IonToolbar, IonTitle, useIonAlert, IonLabel, IonContent, IonGrid, IonButtons, IonMenuButton, IonRow, IonCol, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonItem, IonButton, IonModal, IonIcon } from '@ionic/react';
import ActivitiesContext, { Activity } from '../../components/Actividades/ActividadesConst';
import '../css/Style.css';
import { checkmarkOutline } from 'ionicons/icons';
import swal from 'sweetalert'
const AllActivities: React.FC = () => {

    const alert = (titulo: string, text: string, icon: string, tareas: string, id: string) => {
        swal({
            title: titulo,
            text: text,
            icon: icon,
            buttons: ["no", "si"]
        }).then(respuesta => {
            if (respuesta === true) {
                if (tareas == "completaractividad") {
                    confirmCompletion(id)
                    swal({
                        title: titulo,
                        text: "Completado correctamente",
                        icon: "success",
                        timer: 2000,
                    });
                }
            }
        });
    }
    const [activityToComplete, setActivityToComplete] = useState<Activity>();
    const activitiesCtxt = useContext(ActivitiesContext);
    const confirmCompletion = (activityId: string) => {
        activitiesCtxt.completeActivity(activityId);
    };
    return (
        <React.Fragment>
            <IonModal isOpen={!!activityToComplete}>

            </IonModal>

            <IonPage>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonMenuButton />
                        </IonButtons>
                        <IonTitle>Actividades</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonContent className="center" style={{ position: 'relative', width: '100%', height: '100%' }}>
                    <IonGrid className="ion-grid">{activitiesCtxt.activities.map((activity,index) => (
                        <IonRow key={index}>
                            <IonCol >
                                <IonCard>
                                    <img src={activity.imageUrl} className="img" alt="Activity" />
                                    <IonCardHeader>
                                        <IonCardSubtitle>{activity.hour}</IonCardSubtitle>
                                        <IonCardTitle>{activity.title}</IonCardTitle>
                                    </IonCardHeader>

                                    <IonCardContent>
                                        <p className="TextActive">{activity.description}</p>

                                        {!activity.sugerencia ?
                                            <div>
                                                {!activity.isCompleted ?
                                                    <IonButton fill="outline" shape="round" expand="block" className='CenterElement' onClick={() => alert('' + activity.title, "¿Está seguro de que desea completar esta actividad?", "warning", "completaractividad", activity.id)}>  Completar activida </IonButton>
                                                    :
                                                    <IonIcon color="success" className="CenterElement" icon={checkmarkOutline} />
                                                }
                                            </div>


                                            :
                                            <IonBadge color="primary" className="CenterElement">Es una Sugerencia</IonBadge>
                                        }
                                    </IonCardContent>
                                </IonCard>
                            </IonCol>
                        </IonRow>))}
                    </IonGrid>
                </IonContent>
            </IonPage>
        </React.Fragment>
    );
};

export default AllActivities;