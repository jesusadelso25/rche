import { useContext, useEffect, useState } from 'react';
import './Home.css';
import { add, readerSharp, checkmarkOutline, personCircleSharp, library, barChart, reader } from 'ionicons/icons';
import ActivitiesContext from '../../components/Actividades/ActividadesConst';
import EquiposContext from '../../components/Estadisticas/EstadisticaConst';
import { IonPage, IonHeader, IonBadge, IonFab, IonSelectOption, IonFabButton, IonFabList, IonProgressBar, IonSkeletonText, IonToolbar, IonSelect, IonTitle, IonContent, IonGrid, IonButtons, IonMenuButton, IonRow, IonCol, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent, IonItem, IonButton, IonModal, IonIcon, IonInput } from '@ionic/react';
import swal from 'sweetalert';
import { Line } from '@ant-design/charts';
interface anos {
  Ano: number;
}
var currentTime = new Date();
var year = currentTime.getFullYear();
var AXyearMen = Number(year) - 1;
var AXyearMen2 = Number(year) - 2;

const appanos: anos[] = [
  { Ano: AXyearMen2 },
  { Ano: AXyearMen },
  { Ano: year }
]
const Inicio = () => {
  const alert = (titulo: string, text: string, icon: string, tareas: string, id: string) => {
    swal({
      title: titulo,
      text: text,
      icon: icon,
      buttons: ["no", "si"]
    }).then(respuesta => {
      if (respuesta === true) {
        if (tareas === "completaractividad") {
          confirmCompletion(id)
          swal({
            title: titulo,
            text: "Completado correctamente",
            icon: "success",
            timer: 2000,
          });
        }
      }
    });
  }
  const activitiesCtxt = useContext(ActivitiesContext);
  const Estaditicascont = useContext(EquiposContext);
  const confirmCompletion = (activityId: string) => {
    activitiesCtxt.completeActivity(activityId);
  };
  const [Conteo, getconteo] = useState(false);
  const [selecanos, getselecanos] = useState('');
  var Axselecanos = "";
  useEffect(() => {
    if (selecanos === '') { getselecanos('' + year); }
    if (Conteo === false) {
      obtenerdata("" + year);
    }
    if (selecanos !== '' + Axselecanos) { obtenerdata("" + selecanos); }
  })
  /* Area del control de grafico */
  /* -----contante de los mese que cambia segun la decisiones del usuario */
  const [Enero, setEnero] = useState<Number>(0);
  const [Febrero, setFebrero] = useState<Number>(0);
  const [Marzo, setMarzo] = useState<Number>(0);
  const [Abril, setAbril] = useState<Number>(0);
  const [Mayo, setMayo] = useState<Number>(0);
  const [Junio, setJunio] = useState<Number>(0);
  const [Julio, setJulio] = useState<Number>(0);
  const [Agosto, setAgosto] = useState<Number>(0);
  const [Septiembre, setSeptiembre] = useState<Number>(0);
  const [Octubre, setOctubre] = useState<Number>(0);
  const [Noviembre, setNoviembre] = useState<Number>(0);
  const [Diciembre, setDiciembre] = useState<Number>(0);

  /* -----function de estraer del  arreglo el listado de equipos que tenga el usuario para procesarlo y mostrarla en el grafico */
  async function obtenerdata(Ayer: string) {
    var E = 0, F = 0, May = 0, A = 0, M = 0, J = 0, Jul = 0, A = 0, S = 0, O = 0, N = 0, D = 0;
    setEnero(0); setFebrero(0); setMarzo(0); setAbril(0); setMayo(0); setJunio(0); setJulio(0);
    setAgosto(0); setSeptiembre(0); setOctubre(0); setNoviembre(0); setDiciembre(0);
    Axselecanos = Ayer;
    /*---Calcular el total de consumo por meses segun su nesesidades */
    await Estaditicascont.equipos.filter(Equipos => Equipos.ano.includes(Ayer)).map((Equipos) => {
      if (Equipos.ano === Ayer) {
        switch (Equipos.mes) {
          case 'Enero':
            E += Number(Equipos.totalGasto);
            setEnero(E);
            break;
          case 'Febrero':
            F += Number(Equipos.totalGasto);
            setFebrero(F);
            break;
          case 'Marzo':
            May += Number(Equipos.totalGasto);
            setMarzo(May);
            break;
          case 'Abril':
            A += Number(Equipos.totalGasto);
            setAbril(A);
            break;
          case 'Mayo':
            M += Number(Equipos.totalGasto);
            setMayo(M);
            break;
          case 'Junio':
            J += Number(Equipos.totalGasto);
            setJunio(J);
            break;
          case 'Julio':
            Jul += Number(Equipos.totalGasto);
            setJulio(Jul);
            break;
          case 'Agosto':
            A += Number(Equipos.totalGasto);
            setAgosto(A);
            break;
          case 'Septiembre':
            S += Number(Equipos.totalGasto);
            setSeptiembre(S);
            break;
          case 'Octubre':
            O += Number(Equipos.totalGasto);
            setOctubre(O);
            break;
          case 'Noviembre':
            N += Number(Equipos.totalGasto);
            setNoviembre(N);
            break;
          case 'Diciembre':
            D += Number(Equipos.totalGasto);
            setDiciembre(D);
            break;
        }
      }
      getconteo(true);
    })
  }
  /*lista de informaciones a mostrar */
  const data =
    [
      { year: 'Enero', value: Enero },
      { year: 'Febrero', value: Febrero },
      { year: 'Marzo', value: Marzo },
      { year: 'Abril', value: Abril },
      { year: 'Mayo', value: Mayo },
      { year: 'Junio', value: Junio },
      { year: 'Julio', value: Julio },
      { year: 'Agosto', value: Agosto },
      { year: 'Septiembre', value: Septiembre },
      { year: 'Octubre', value: Octubre },
      { year: 'Noviembre', value: Noviembre },
      { year: 'Diciembre', value: Diciembre },
    ];
  /* configuracion para mostrar en el grafico */
  const config = {
    data,
    width: 200,
    height: 300,
    xField: 'year',
    yField: 'value',
    color: '#428CFF',
    point: {
      size: 7,
      shape: 'circle',
      style: {
        fill: '#428CFF',
        stroke: 'white',
      }
    },
  };
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
          <IonButton className="ion-float-right salir" fill="clear" routerLink="../ActUser" ><IonIcon slot="start" ios={personCircleSharp} />  </IonButton>
          <div style={{ paddingTop: '15px' }} >
            <IonTitle>INICIO</IonTitle>
          </div>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen className='Content'>
        <div className='Grafic' >
          <IonTitle className="centerItem">Grafico del Consumo del hogar</IonTitle>
          <IonSelect className="centerItem" value={selecanos} placeholder="Select horas" onIonChange={(e: any) => getselecanos(e.detail.value)}>
            {appanos.map((ano, i) =>
              <IonSelectOption key={i} value={'' + ano.Ano}> Años {ano.Ano} </IonSelectOption>
            )}
          </IonSelect>
          < Line {...config} />
        </div>
        <div className="center">
          <IonTitle className="center">Recomendaciones y Actividades del hogar</IonTitle>

          <IonGrid className="ion-grid">
            {activitiesCtxt.activities.map((activity, index) => (
              <IonRow key={index}>
                <IonCol className="ion-text-center">
                  <IonCard>
                    <img src={activity.imageUrl} className="img" alt="Activity" />
                    <IonCardHeader>
                      <IonCardSubtitle>{activity.hour}</IonCardSubtitle>
                      <IonCardTitle>{activity.title}</IonCardTitle>
                    </IonCardHeader>
                    <IonCardContent>
                      <p className="TextActive">{activity.description}</p>

                      {!activity.sugerencia ?
                        <div>
                          {!activity.isCompleted ?
                            <IonButton fill="outline" shape="round" expand="block" className='CenterElement' onClick={() => alert('' + activity.title, "¿Está seguro de que desea completar esta actividad?", "warning", "completaractividad", activity.id)}>  Completar activida </IonButton>
                            :
                            <IonIcon color="success" className="CenterElement" icon={checkmarkOutline} />
                          }
                        </div>
                        :
                        <IonBadge color="primary" className="CenterElement">Es una Sugerencia</IonBadge>
                      }
                    </IonCardContent>
                  </IonCard>
                </IonCol>
              </IonRow>))}
          </IonGrid>
          <div style={{ padding: "28px", paddingLeft: "0px", paddingRight: "0px" }}>
            <IonButton fill="solid" color="success" expand="full" className='listaInicio' routerLink='/Actividades'><IonIcon className='listaInicioinco' icon={reader} />.Lista de Actividades</IonButton>
            <IonButton fill="solid" color="danger" expand="full" className='listaInicio' routerLink='/Estaditica' ><IonIcon className='listaInicioinco' icon={barChart} />.Lista de consumo de Equipos</IonButton>
            <IonButton fill="solid" color="warning" expand="full" className='listaInicio' routerLink='/Simulador' ><IonIcon className='listaInicioinco' icon={readerSharp} />.Lista de equipos de simulacion </IonButton>
            <IonButton fill="solid" color="primary" expand="full" className='listaInicio' routerLink='/Manual' ><IonIcon className='listaInicioinco' icon={library} />.Manual para principiantes</IonButton>
          </div>
        </div>
        <IonFab vertical="bottom" horizontal="end" slot="fixed" >
          <IonFabButton>
            <IonIcon icon={add} />
          </IonFabButton>
          <IonFabList side="top">
            <IonFabButton color="success" routerLink={"/AddActividades"}>
              <IonIcon icon={add} />
            </IonFabButton>
          </IonFabList>
          <IonFabList side="start">
            <IonFabButton color="warning" routerLink={"/Actividades"} >
              <IonIcon icon={readerSharp} />
            </IonFabButton>
          </IonFabList>
        </IonFab>
      </IonContent>
    </IonPage>
  )
}

export default Inicio