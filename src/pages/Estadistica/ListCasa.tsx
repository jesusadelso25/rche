import { useContext, useState, useEffect } from 'react'
import { add } from 'ionicons/icons';
import EquiposContext from '../../components/Estadisticas/EstadisticaConst';
import '../css/Style.css'
import swal from 'sweetalert'
import { IonSegment, IonIcon, IonSearchbar, IonSegmentButton, IonButton, IonButtons, IonContent, IonHeader, IonLabel, IonMenuButton, IonPage, IonTitle, IonToolbar, IonGrid, IonRow, IonCol, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';

const Estaditica = () => {
    const equiposCtxt = useContext(EquiposContext);
    var currentTime = new Date();
    var yearl = currentTime.getFullYear();
    const [Year, getYear] = useState("");
    const [activarboton, setactivarboton] = useState(false);
    const [searchText, setsearchText] = useState("");
    const [busqueda, setbussqueda] = useState('');
    const [total, SetTotal] = useState<Number>(1);
    /*alert de Msg */
    const alert = (titulo: string, text: string, icon: string, tareas: string, id: string) => {
        swal({
            title: titulo,
            text: text,
            icon: icon,
            buttons: ["no", "si"]
        }).then(respuesta => {
            if (respuesta === true) {
                if (tareas == "completaractividad") {
                    Elimcasa(id)
                    swal({
                        title: titulo,
                        text: "Se Elimino Correctamente",
                        icon: "success",
                        timer: 1000,
                    });
                }
            }
        });
    }


    async function AnteSumar() {
        var R2 = 0;
        SetTotal(0);
        await equiposCtxt.equipos.filter(Equiposl => Equiposl.ano.includes(Year)).map((Dip) => {
            if (Dip.ano === Year) {
                
                R2 += Number(Dip.totalGasto);
                SetTotal(R2);
            }
        });
        setactivarboton(false);
    }

    useEffect(() => {
        getYear('' + yearl);
        if (Number(total) <=1) {
            AnteSumar();
        }
        if (activarboton) {
             setInterval((()=>{
                SetTotal(0);
             }),2000)
        }
    })


    async function Elimcasa(Id: string) {
        setactivarboton(true);
        if (Id) {
            try {
                await equiposCtxt.elimeqiponube(Id);
            } catch (e) {
                swal({
                    title: "Error",
                    text: "Ocurrio un Error en el proceso",
                    icon: "warning",
                })
            }
        }
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonMenuButton />
                    </IonButtons>
                    <IonTitle>Consumo del hogar</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen style={{ position: 'relative', width: '100%', height: '100%' }}>
                <IonRow key='Lstnivel'>
                    <IonCol className='center'>
                        <IonSearchbar value={searchText} placeholder='Buscar por Nombre ' onIonChange={e => setsearchText(e.detail.value!)}></IonSearchbar>
                        <IonSegment value={busqueda} placeholder="Select" onIonChange={(e: any) => setbussqueda(e.detail.value)} >
                            <IonSegmentButton value=''>
                                <IonLabel>Todos</IonLabel>
                            </IonSegmentButton>
                            <IonSegmentButton value='Alto'>
                                <IonLabel >Alto</IonLabel>
                            </IonSegmentButton>
                            <IonSegmentButton value='Medio'>
                                <IonLabel>Medio </IonLabel>
                            </IonSegmentButton>
                            <IonSegmentButton value='Bajo'>
                                <IonLabel>Bajo</IonLabel>
                            </IonSegmentButton>
                        </IonSegment>
                    </IonCol>
                </IonRow>
                <IonGrid >
                    <div className="center">
                        <IonButton fill='outline' expand='block' shape="round" color="--ion-color-primary" routerLink={"simulador"} ><IonIcon /><IonIcon /> <IonIcon icon={add} />Agregar nuevo consumo </IonButton>
                        <IonTitle >Total RD$ {total} Año:{Year} </IonTitle>
                    </div>
                </IonGrid>
                {
                    searchText ?
                        <div>
                            <IonGrid className="ion-grid" >
                                {equiposCtxt.equipos.filter(Equipos => Equipos.Equipo.toUpperCase().includes(searchText.toUpperCase()) && Equipos.ano.includes(Year)).map((Equipos, index) =>
                                    <IonRow key={index}>
                                        <IonCol className="ion-text-center">
                                            <IonCard>
                                                <h5 className='Titulo'>Equipo:{Equipos.Equipo}</h5>
                                                <img className="img" src={Equipos.imageUrl} />
                                                <IonCardHeader>
                                                    <h4 className="justificar">
                                                        Marca del equipo {Equipos.Marca}, Consumo encedido: {Equipos.voltaje} Wh
                                                        Semana de Uso: {Equipos.SemanaUso}, Horas: {Equipos.hour} y Dias:{Equipos.diasUso} de Uso
                                                        Gasto total es: {Equipos.totalGasto} peso Dominicanos del Mes: {Equipos.mes} del Año: {Equipos.ano}
                                                    </h4>
                                                </IonCardHeader>
                                                <IonButton fill="outline" color="danger" shape="round" className='CenterElement' onClick={() => alert('' + Equipos.Equipo, "¿Está seguro de que desea Eliminar el Equipo?", "warning", "completaractividad", Equipos.id)}>  <IonIcon icon='trash' />Eliminar </IonButton>
                                            </IonCard>
                                        </IonCol>
                                    </IonRow>)}
                            </IonGrid>

                        </div> :
                        <div>
                            {busqueda ?
                                <div>
                                    <IonGrid className="ion-grid" >
                                        {equiposCtxt.equipos.filter(Equipos => Equipos.nivelvoltaje.includes(busqueda) && Equipos.ano.includes(Year)).map((Equipos, index) =>
                                            <IonRow key={index}>
                                                <IonCol className="ion-text-center">
                                                    <IonCard>
                                                        <h5 className='Titulo'>Equipo:{Equipos.Equipo}</h5>
                                                        <img className="img" src={Equipos.imageUrl} />
                                                        <IonCardHeader>
                                                            <h4 className="justificar">
                                                                Marca del equipo {Equipos.Marca}, Consumo encedido: {Equipos.voltaje} Wh
                                                                Semana de Uso: {Equipos.SemanaUso}, Horas: {Equipos.hour} y Dias:{Equipos.diasUso} de Uso
                                                                Gasto total es: {Equipos.totalGasto} peso Dominicanos del Mes: {Equipos.mes} del Año: {Equipos.ano}
                                                            </h4>
                                                        </IonCardHeader>
                                                        <IonButton fill="outline" color="danger" shape="round" className='CenterElement' onClick={() => alert('' + Equipos.Equipo, "¿Está seguro de que desea Eliminar el Equipo?", "warning", "completaractividad", Equipos.id)}>  <IonIcon icon='trash' />Eliminar </IonButton>
                                                    </IonCard>
                                                </IonCol>
                                            </IonRow>)}
                                    </IonGrid>
                                </div>
                                :
                                <div>
                                    <IonGrid className="ion-grid" >
                                        {equiposCtxt.equipos.filter(Equipos => Equipos.ano.includes(Year)).map((Equipos, index) =>
                                            <IonRow key={index}>
                                                <IonCol className="ion-text-center">
                                                    <IonCard>
                                                        <h5 className='Titulo'>Equipo:{Equipos.Equipo}</h5>
                                                        <img className="img" src={Equipos.imageUrl} />
                                                        <IonCardHeader>
                                                            <h4 className="justificar">
                                                                Marca del equipo {Equipos.Marca}, Consumo encedido: {Equipos.voltaje} Wh
                                                                Semana de Uso: {Equipos.SemanaUso}, Horas: {Equipos.hour} y Dias:{Equipos.diasUso} de Uso
                                                                Gasto total es: {Equipos.totalGasto} peso Dominicanos del Mes: {Equipos.mes} del Año: {Equipos.ano}
                                                            </h4>
                                                        </IonCardHeader>
                                                        <IonButton fill="outline" color="danger" shape="round" className='CenterElement' onClick={() => alert('' + Equipos.Equipo, "¿Está seguro de que desea Eliminar el Equipo?", "warning", "completaractividad", Equipos.id)}>  <IonIcon icon='trash' />Eliminar </IonButton>
                                                    </IonCard>
                                                </IonCol>
                                            </IonRow>)}
                                    </IonGrid>
                                </div>
                            }
                        </div>}
            </IonContent>
        </IonPage>
    )
}
export default Estaditica