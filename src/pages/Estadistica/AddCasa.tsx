import { calculator, arrowBack, cut, home } from 'ionicons/icons';
import EquiposContext from '../../components/EquiposElectr/EquipoConst';
import EquiposcasaContext, { NivelVoltaje } from '../../components/Estadisticas/EstadisticaConst';
import suplidorContext from '../../components/SuplEnergia/SuplodorCont';
import userContext from '../../components/Usuario/UsuarioCont';
import '../css/Style.css'
import { IonItem, IonSelectOption, IonSelect, IonIcon, IonHeader, IonButtons, IonTitle, IonToolbar, IonCardContent, IonButton, IonContent, IonLabel, IonPage, IonGrid, IonRow, IonCol, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';
import { useState, useContext, useEffect } from 'react';
import { useParams } from "react-router";
import swal from 'sweetalert'

interface Meses {
    Mes: string;
}
interface anos {
    Ano: number;
}
var currentTime = new Date();
var year = currentTime.getFullYear();
var AXyearMen = Number(year)-1;

const appanos: anos[] = [
    { Ano: AXyearMen },
    { Ano: year },
]
const appmese: Meses[] = [
    { Mes: 'Enero' },
    { Mes: 'Febrero' },
    { Mes: 'Marzo' },
    { Mes: 'Abril' },
    { Mes: 'Mayo' },
    { Mes: 'Junio' },
    { Mes: 'Julio' },
    { Mes: 'Agosto' },
    { Mes: 'Septiembre' },
    { Mes: 'Octubre' },
    { Mes: 'Noviembre' },
    { Mes: 'Diciembre', }
];
const AddEsta: React.FC = () => {
    const equiposCtxt = useContext(EquiposContext);
    const equiposcasaCtxt = useContext(EquiposcasaContext);
    const UserRefCtxt = useContext(userContext);

    const [selecHora, getselecHora] = useState('');
    const [selecsemana, getselecsemana] = useState('');
    const [selecdias, getselecdias] = useState('');
    const [selecmeses, getselecmese] = useState('');
    const [selecanos, getselecanos] = useState('');
    const [AXVoltaje, SetAXVoltaje] = useState('');

    const [ResultadoCalculo, setResultadoCalculo] = useState<Number>(0);

    const [activarboton, setactivarboton] = useState(false);
    const [activarboton2, setactivarboton2] = useState(false);
    const suplidorasCtxt = useContext(suplidorContext);
    const [idsuplidor, setidsuplidor] = useState(false);
    const [Nombresuplidor, setNombresuplidor] = useState('');
    const [Preciosuplidor, setPreciosuplidor] = useState('');
    const alert = (titulo: string, text: string, icon: string) => {
        swal({
            title: titulo,
            text: text,
            icon: icon,
        });
    }

    async function Bucarinfor() {
        if (idsuplidor) { } else {
            {
                UserRefCtxt.UserInfo.map(userInf => {
                    buscarprecio(userInf.idSuplidora);
                    setidsuplidor(true);
                }
                )
            }
        }
    }
    async function buscarprecio(id: string) {
        {
            suplidorasCtxt.suplidoras.filter(Suplidoras => Suplidoras.id.includes(id)).map(Suplidora => {
                setPreciosuplidor('' + Suplidora.precio);
                setNombresuplidor('' + Suplidora.suplidora);
            }
            )
        }
    }
    type QuizParams = {
        id: string;
    };
    const { id } = useParams<QuizParams>()
    const pemaddactiidades = () => {
        if (activarboton != null) {
            setactivarboton(true);
        } else {
            setactivarboton(true);
        }
    }

    const addequiposcasa = (Equipo: string, Marca: string, nivelvoltaje: string, voltaje: string, imageUrl: string) => {

        if (selecHora && selecsemana && selecdias && selecmeses && selecanos) {
            equiposcasaCtxt.addaddequiposnube(Equipo, Marca, selecHora, selecdias, selecsemana, '' + ResultadoCalculo, nivelvoltaje as NivelVoltaje, voltaje, imageUrl, selecmeses, ""+selecanos);
            alert('Bien :) ', 'se guardo correctamente!', 'success');
        } else {
            alert('Error :(', 'Favor de selecionar los campos correpondiente Horas,semana,dias', 'warning');
        }


    }
    useEffect(() => {
        Bucarinfor();
        if (Number(ResultadoCalculo) > 0) {
            if (Number(AXVoltaje) > 0) {
                calcular(AXVoltaje)
            }

        } else {
            if (Number(selecHora) > 0) {
                if (Number(selecdias) > 0) {
                    if (Number(selecsemana) > 0) {
                        pemaddactiidades();
                    }
                }
            }
        }
    })
    function calcular(voltajeR: string) {
        SetAXVoltaje(voltajeR);
        if (Number(selecHora) > 0) {
            if (Number(selecdias) > 0) {
                if (Number(selecsemana) > 0) {
                    if (Number(Preciosuplidor) > 0) {
                        var resultado = ((((Number(selecHora) * Number(selecsemana)) * Number(selecdias)) * Number(voltajeR)) * Number(Preciosuplidor));
                        setResultadoCalculo(resultado)
                    } else {
                        alert('informacion :(', 'Estamos buscando su provedor precione de nuevo en calcular', 'warning');
                    }
                }
            } else {
                alert('Error :(', 'los dias debe ser mayor a Cero', 'warning');
            }
        } else {
            alert('Error :(', 'Si no tienes horas  en la semana, por ley es resultado es cero', 'warning');
        }
        setactivarboton2(true);
    }
    function Limpiarcalculo() {
        setactivarboton(false);
        setactivarboton2(false);
        getselecHora('');
        getselecdias('');
        getselecsemana('');
        setResultadoCalculo(0);
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonButton routerLink={"/Simulador"}><IonIcon icon={arrowBack} /></IonButton>
                    </IonButtons>
                    <div style={{ textAlign: "center", position: "relative" }}>
                        <IonLabel>Suplidor {Nombresuplidor}</IonLabel><br></br>
                        <IonLabel>Precio RD${Preciosuplidor} </IonLabel><br></br>
                    </div>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen style={{ position: 'relative', width: '100%', height: '100%' }}>
                <div className="center" >
                    <div> {equiposCtxt.equipos.filter(Equipos => Equipos.id.includes(id)).map((Equipos) =>
                        <IonGrid className="ion-grid1"  >
                            <IonRow key={Equipos.id}>
                                <IonCol className="center">
                                    <IonCard>
                                        <IonTitle>Equipo:{Equipos.Equipo}</IonTitle>
                                        <img className="img" src={Equipos.imageUrl} />
                                        <IonCardHeader>
                                            <IonCardTitle>Marca:{Equipos.Marca}</IonCardTitle>
                                            <IonCardTitle >Consumo encedido:{Equipos.voltaje}Wh</IonCardTitle>
                                        </IonCardHeader>
                                        <IonCardContent>
                                            <IonItem >
                                                <IonLabel>Horas</IonLabel>
                                                <IonSelect value={selecHora} placeholder="Select horas" onIonChange={(e: any) => getselecHora(e.detail.value)}>
                                                    {Array(25).fill(1).map((_, i) =>
                                                        <IonSelectOption key={i} value={i}>{i} Horas</IonSelectOption>
                                                    )}
                                                </IonSelect>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Dias de uso</IonLabel>
                                                <IonSelect value={selecdias} placeholder="Select horas" onIonChange={(e: any) => getselecdias(e.detail.value)}>
                                                    {Array(8).fill(1).map((_, i) =>
                                                        <IonSelectOption key={i} value={i}>{i} Dias</IonSelectOption>
                                                    )}
                                                </IonSelect>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Semana</IonLabel>
                                                <IonSelect value={selecsemana} placeholder="Select horas" onIonChange={(e: any) => getselecsemana(e.detail.value)}>
                                                    {Array(5).fill(1).map((_, i) =>
                                                        <IonSelectOption key={i} value={i}>{i} Semana</IonSelectOption>
                                                    )}
                                                </IonSelect>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Meses</IonLabel>
                                                <IonSelect value={selecmeses} placeholder="Select horas" onIonChange={(e: any) => getselecmese(e.detail.value)}>
                                                    {appmese.map((mese, i) =>
                                                        <IonSelectOption key={i} value={mese.Mes}>{mese.Mes}</IonSelectOption>
                                                    )}
                                                </IonSelect>
                                            </IonItem>
                                            <IonItem>
                                                <IonLabel>Años</IonLabel>
                                                <IonSelect value={selecanos} placeholder="Select horas" onIonChange={(e: any) => getselecanos(e.detail.value)}>
                                                    {appanos.map((ano, i) =>
                                                        <IonSelectOption key={i} value={ano.Ano}>{ano.Ano} </IonSelectOption>
                                                    )}
                                                </IonSelect>
                                            </IonItem>
                                            <IonCardTitle><h3>Resultado RD$ {ResultadoCalculo}</h3></IonCardTitle>
                                            <IonButton fill="outline" color="danger" shape="round" onClick={Limpiarcalculo} disabled={!activarboton2} >  <IonIcon icon={cut} />Limpiar </IonButton>
                                            <IonButton fill="outline" color="--ion-color-primary" shape="round" onClick={() => calcular(Equipos.voltaje)} disabled={!activarboton} >  <IonIcon icon={calculator} /> calcular </IonButton>
                                            <IonButton fill="outline" color="--ion-color-primary" shape="round" onClick={() => addequiposcasa
                                                (
                                                    Equipos.Equipo, Equipos.Marca, Equipos.nivelvoltaje, Equipos.voltaje, Equipos.imageUrl
                                                )} disabled={!activarboton2} >  <IonIcon icon={home} /> Agregar </IonButton>
                                            <IonButton fill="outline" color="--ion-color-primary" shape="round" routerLink={"../Estaditica"}  >  <IonIcon icon={home} />...Lista </IonButton>
                                        </IonCardContent>
                                    </IonCard>
                                </IonCol>
                            </IonRow>
                        </IonGrid>

                    )}
                    </div>
                </div>
            </IonContent>
        </IonPage>
    )
}
export default AddEsta