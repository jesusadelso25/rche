import React, { useContext, useEffect, useState } from 'react'

/* importado datos de imagenes y estilo*/
import './Style.css';
import { arrowBack, createOutline } from 'ionicons/icons';
import { IonIcon, IonInput, IonButton, IonContent, IonSelectOption, IonCheckbox, IonSelect, IonLabel, IonPage, IonHeader, IonButtons, IonMenuButton, IonToolbar, IonTitle } from '@ionic/react';

/*importado directorio de alertas */
import swal from 'sweetalert'
import suplidorContext from '../../components/SuplEnergia/SuplodorCont';
import userContext from '../../components/Usuario/UsuarioCont';

const ActUser: React.FC = () => {
    const suplidorasCtxt = useContext(suplidorContext);
    const UserRefCtxt = useContext(userContext);
    var [Opcional, setopcional] = useState(false);
    var [Opcional2, setopcional2] = useState(false);
    const [Suplidora, setSuplidora] = useState('');
    /* informacion requerida para el actualizar */
    const  [usernam, setUsernam] = useState('');
    const [Password, setPassword] = useState('');
    const  [CPassword, setCPassword] = useState<string>('');
    const  [CompPassword, setCompCPassword] = useState<string>('');
    const [Chec, setChec] = useState(false)
    const [AxCheck, setAxCheck] = useState(0);
    const [Chec2, setChec2] = useState(false)
    function Bucarinfor() {
        {
            UserRefCtxt.UserInfo.map(userInf => {
                setSuplidora(userInf.idSuplidora);
                setUsernam(userInf.Email);
            }
            )
        }
    }
    function Cambiar(id: string) {
        setAxCheck(1);
        if (id === '1') {
            if (Chec) {
                setChec(false);
            } else {
                setChec(true);
            }
        }
        if (id === '2') {
            if (Chec2) {
                setChec2(false);
            } else {
                setChec2(true);

            }

        }

    }
    useEffect(() => {
        Bucarinfor();
        if (AxCheck > 0) {
            setAxCheck(0);
            if (Chec) {
                setopcional(true);

            } else {
                setopcional(false);
                setChec(false);
            }
            if (Chec2) {
                setopcional2(true);
            } else {
                setopcional2(false);
                setChec2(false);
            }

        }


    })
    const mostraralerta = (title: string, text: string, icon: string, timer: number) => {
        swal({
            title: title,
            text: text,
            icon: icon,
            timer: timer,
        });
    }
    const peticiones = async () => {
        if (Chec) {
            if (Chec2) {
                if (Password !== CPassword) { mostraralerta('Avertecia', 'Los Password son diferentes', 'warning', 2000) }
                else {
                    if (Password !== CompPassword) {
                        if (usernam?.trim() === '' || Password?.trim() === '' || Suplidora?.trim() === '') {
                            mostraralerta('Avertecia', 'llenas todos los campos 3 campos requeridos', 'warning', 2000)
                        } else {
                            mostraralerta('Avertecia', 'Este servicio esta en mantenimiento, No se puede realizar cambios', 'warning', 20000)
                        }
                    } else {
                        mostraralerta('Avertecia', 'Tu contraseña actual es la misma que na nueva', 'warning', 2000)
                    }

                }

            } else {
                if (Password !== CPassword) { mostraralerta('Avertecia', 'Los Password son diferentes', 'warning', 2000) }
                else {
                    if (usernam?.trim() === '' || CompPassword?.trim() === '' || Password?.trim() === '') {
                        mostraralerta('Avertecia', 'llenas todos los campos 2 campos requeridos', 'warning', 2000)
                    } else {
                        if (Password !== CompPassword) {
                            mostraralerta('Avertecia', 'Este servicio esta en mantenimiento se puede realizar cambios', 'warning', 2000)
                        } else {
                            mostraralerta('Avertecia', 'Tu contraseña actual es la misma que na nueva', 'warning', 2000)
                        }
                    }
                }
            }
        } else {
            if (Chec2) {
                if (usernam?.trim() === '' || Suplidora?.trim() === '') {
                    mostraralerta('Avertecia', 'llenas todos los campos 2 campos requeridos', 'warning', 2000)
                } else {
                    mostraralerta('Avertecia', 'Este servicio esta en mantenimiento, No se puede realizar cambios', 'warning', 20000)
                }
            } else {
                if (usernam?.trim() === '') {
                    mostraralerta('Avertecia', 'llenas todos los campos Email', 'warning', 2000)
                } else {
                    mostraralerta('Avertecia', 'Este servicio esta en mantenimiento, No se puede realizar cambios', 'warning', 20000)
                }
            }
        }
    }
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonButton  routerLink={"/Inicio"}><IonIcon icon={arrowBack} /></IonButton>
                    </IonButtons>
                    <IonTitle>Actualizar Usuario</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen >
                <body className="fullscreen" style={{ position: 'relative', width: '100%', height: '100%' }}>
                    <div className="Contextform" >
                        <IonTitle className="Contexttitulo" >Actualizar Registro</IonTitle>
                        <IonButton fill='outline' shape="round" onClick={() => Cambiar('1')}>Actualizar Password.. <IonCheckbox checked={Chec} onIonChange={(e: any) => setChec(e.target.value)} ></IonCheckbox></IonButton> <br></br>
                        <IonButton fill='outline' shape="round" onClick={() => Cambiar('2')}>Actualizar Suplidora..  <IonCheckbox checked={Chec2} onIonChange={(e: any) => setChec2(e.target.value)} ></IonCheckbox></IonButton> <br></br>
                        <br></br> <IonLabel position='floating' >Email*</IonLabel>
                        <IonInput className="ContextInput" value={usernam} type="email" onIonChange={(e: any) => setUsernam(e.target.value)} placeholder="Usuername@gmail.com?" required></IonInput>
                        {Opcional ?
                            <div>
                                <IonLabel position='floating'>Password*</IonLabel>
                                <IonInput className="ContextInput" type="password" onIonChange={(e: any) => setPassword(e.target.value)} placeholder="Password?" required ></IonInput>
                                <IonLabel >Confirm Password*</IonLabel>
                                <IonInput className="ContextInput" type="password" onIonChange={(e: any) => setCPassword(e.target.value)} placeholder="Password?" required ></IonInput>
                            </div>

                            :
                            <div>

                            </div>
                        }

                        {Opcional2 ?
                            <div className="ion-text-center">
                                <IonLabel>Suplidoras de Energia</IonLabel>
                                <IonSelect value={Suplidora} placeholder="Select" onIonChange={(e: any) => setSuplidora(e.detail.value)}>
                                    {suplidorasCtxt.suplidoras.map(Suplidora =>
                                        <IonSelectOption value={Suplidora.id} >{Suplidora.suplidora}</IonSelectOption>
                                    )}
                                </IonSelect>
                            </div>
                            :
                            <div>
                            </div>

                        }
                        <br></br>
                        <IonLabel >Password Actual*</IonLabel>
                        <IonInput className="ContextInput" type="password" onIonChange={(e: any) => setCompCPassword(e.target.value)} placeholder="Password?"></IonInput>
                        <IonButton fill="outline" shape="round" onClick={() => peticiones()}><IonIcon icon={createOutline} />..Actualizar</IonButton>
                    </div>
                </body>
            </IonContent>
        </IonPage>
    )
}

export default ActUser