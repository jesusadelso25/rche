import React, { useContext, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
/* importado datos de imagenes y estilo*/
import './Style.css';
import log from '../../components/asset/image/icon.png';
import { enterOutline, personAdd } from 'ionicons/icons';
import { IonIcon, IonInput, IonButton, IonContent, IonSelectOption, IonProgressBar, IonSelect, IonLabel, IonPage, IonTitle } from '@ionic/react';

/*importado conexion  con firebase */

import fb from '../../components/BDconfig/firebaseConfig'

import suplidorContext from '../../components/SuplEnergia/SuplodorCont';
import UsuarioC from '../../components/Usuario/UsuarioCont';
const Login: React.FC = () => {
    const direct = useHistory();
    const suplidorasCtxt = useContext(suplidorContext);
    const UsuarioCont = useContext(UsuarioC);
    const [selection, setselection] = useState(true);
    const [username, setUsername] = useState('');
    const [Password, setPassword] = useState('');
    const [CPassword, setCPassword] = useState<string>('');
    const [Suplidora, setSuplidora] = useState('');
    const [estado, setestado] = useState(0);
    const [activarboton, getactivarboton] = useState(false);
    const [activarboton2, getactivarboton2] = useState(false);
    const mostraralerta = (title: string, text: string, icon: string, timer: number) => {
        fb.swal({
            title: title,
            text: text,
            icon: icon,
            timer: timer,
        });
    }
    useEffect(() => {
        if (username && Password) {
            getactivarboton(true)
        } else {
            getactivarboton(false)
        }
        if (username && Password && CPassword && Suplidora) {
            getactivarboton2(true)
        } else {
            getactivarboton2(false)
        }
    })
    const peticiones = async (peticion: string) => {
        if (peticion === "register") {
            /*validad los datos del regist */
            if (Password !== CPassword) {
                mostraralerta('Avertecia', 'Los Password son diferentes', 'warning', 2000)
            }
            else {
                if (username?.trim() === '' || Password?.trim() === '' || Suplidora.trim() === '') {
                    mostraralerta('Avertecia', 'llenas todos los campos', 'warning', 2000)
                } else {
                    setestado(0.5)
                    UsuarioCont.Crearusuario(username, Password,Suplidora)  
                    setestado(0.8)
                }
            }

        } else if (peticion === 'login') {
            if (username?.trim() === '' || Password?.trim() === '') {
                mostraralerta('Avertecia', 'Llena los campos!', 'warning', 2000)
            } else {
                UsuarioCont.Validarusuario(username, Password);

            }
        }

    }
    return (
        <IonPage>
            <IonContent fullscreen >
                <body className="fullscreen" style={{ position: 'relative', width: '100%', height: '100%' }}>
                    <IonProgressBar value={estado}></IonProgressBar><br />
                    <div className="Contextform"  >
                        <img src={log} className="ContextLogo"></img><br></br>
                        <IonTitle className="Contexttitulo" >{selection ? "login" : "Register"}</IonTitle>
                        <IonLabel position='floating' >Email</IonLabel>
                        <IonInput className="ContextInput" type="email" onIonChange={(e: any) => setUsername(e.target.value)} placeholder="Usuername@gmail.com?" required></IonInput>
                        <IonLabel position='floating'>Password</IonLabel>
                        <IonInput className="ContextInput" type="password" onIonChange={(e: any) => setPassword(e.target.value)} placeholder="Password?" required ></IonInput>

                        {selection ?
                            <div>
                                <IonButton shape="round" onClick={() => peticiones('login')} disabled={!activarboton}><IonIcon icon={enterOutline} />..Login</IonButton>
                                <p> New Here?<a onClick={() => setselection(false)}>Register</a></p>
                            </div>
                            :
                            <div>
                                <IonLabel >Confirm Password</IonLabel>
                                <IonInput className="ContextInput" type="password" onIonChange={(e: any) => setCPassword(e.target.value)} placeholder="Password?"></IonInput>
                                <div>
                                    <div className="ion-text-center">
                                        <IonLabel>Suplidoras de Energia</IonLabel>
                                        <IonSelect value={Suplidora} placeholder="Select" onIonChange={(e: any) => setSuplidora(e.detail.value)}>
                                            {suplidorasCtxt.suplidoras.map(Suplidora =>
                                                <IonSelectOption value={Suplidora.id} >{Suplidora.suplidora}</IonSelectOption>
                                            )}
                                        </IonSelect>
                                    </div>
                                </div>
                                <IonButton shape="round" onClick={() => peticiones('register')} disabled={!activarboton2}><IonIcon icon={personAdd} />Register</IonButton>
                                <p> Already have an account?<a onClick={() => setselection(true)}>Login</a></p>
                            </div>
                        }
                    </div>
                </body>
            </IonContent>
        </IonPage>
    )
}

export default Login