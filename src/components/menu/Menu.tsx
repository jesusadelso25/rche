import { useHistory, useLocation } from 'react-router-dom'
import {
  IonContent,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonMenu,
  IonMenuToggle,
  IonNote,
  IonCol,
  IonButton,
} from '@ionic/react';
import './Menu.css';
import { archiveOutline, barChart, calculator, power, footstepsSharp, heartOutline, mailOutline, paperPlaneOutline, paperPlaneSharp, reader, library, enterOutline } from 'ionicons/icons';
import React, { useState, useContext, useEffect } from 'react';
import fb from '../BDconfig/firebaseConfig';
interface AppPage {
  url: string;
  iosIcon: string;
  mdIcon: string;
  title: string;
}
const appPages: AppPage[] = [
  {
    title: 'INICIO',
    url: '/Inicio',
    iosIcon: mailOutline,
    mdIcon: reader
  },
  {
    title: 'Actividades',
    url: '/AddActividades',
    iosIcon: mailOutline,
    mdIcon: footstepsSharp
  },
  {
    title: 'Estaditicas',
    url: '/Estaditica',
    iosIcon: paperPlaneOutline,
    mdIcon: barChart
  }
  ,
  {
    title: 'Simulador',
    url: '/Simulador',
    iosIcon: heartOutline,
    mdIcon: calculator
  }
];
const Menu: React.FC = () => {
  const direct = useHistory();
  const location = useLocation();
  const [activarboton, setactivarboton] = useState(false);
  const [activarbotonlogin, setactivarlogin] = useState(false);
  const [email, setemail] = useState('');
  const [IdUser, setIdUser] = useState('');

  useEffect(() => {
    if (activarboton) {
        if (location.pathname === "/login") { direct.push("/Inicio"); }
    }
    else {
      setactivarlogin(true);
      const validar = fb.onAuthStateChanged(fb.auth, (usuariofirebase) => {
        if (usuariofirebase) {
          const username = {
            uid: usuariofirebase.uid,
            email: usuariofirebase.email,
          };
          setactivarboton(true);
          setactivarlogin(false);
          setemail("" + username.email);
          setIdUser("" + username.uid);

        } else {
          if (location.pathname === "/login") { } else {
            if (location.pathname === "/Manual") { } else {
              if (location.pathname === "/Nosotros") { } else {
                direct.push("/login");
              }
            }
          }

        }
      });
    }


  }, [])

  const salir = () => {
    fb.swal({
      title: "SALIR",
      text: "Esta seguro que decea salir",
      icon: "warning",
      buttons: ["no", "si"]
    }).then(respuesta => {
      if (respuesta === true) {
        setactivarboton(false);
        setactivarlogin(true);
        fb.signOut(fb.auth);
        setemail("");
        direct.push("/login");
      }
    });
  }

  return (
    <IonMenu contentId="main" type="overlay" >
      <IonContent>
        <IonButton disabled={!activarboton} className="ion-float-right salir" fill="clear" onClick={() => salir()} routerDirection="none" ><IonIcon slot="start" ios={power} color='danger' /></IonButton>
        <IonList id="inbox-list">
          <IonListHeader>Bienvenido</IonListHeader>
          <div className='user'>
            <IonNote>{email} </IonNote>
          </div>

          <IonCol >
            <IonButton className="ion-float-right" fill="clear">
            </IonButton>
          </IonCol>
          {appPages.map((appPage, index) => {
            return (
              <IonMenuToggle key={index} autoHide={false}>
                <IonItem className={location.pathname === appPage.url ? 'selected' : ''} routerLink={appPage.url} routerDirection="none" lines="none" detail={false} disabled={!activarboton}>
                  <IonIcon slot="start" ios={appPage.iosIcon} md={appPage.mdIcon} />
                  <IonLabel>{appPage.title}</IonLabel>
                </IonItem>
              </IonMenuToggle>
            );
          })}
          <IonItem className={location.pathname === "/Nosotros" ? 'selected' : ''} routerLink={"/Nosotros"} routerDirection="none" lines="none" detail={false}>
            <IonIcon slot="start" ios={archiveOutline} md={paperPlaneSharp} />
            <IonLabel>Nosotros</IonLabel>
          </IonItem>
          <IonItem className={location.pathname === "/Manual" ? 'selected' : ''} routerLink={"/Manual"} routerDirection="none" lines="none" detail={false}>
            <IonIcon slot="start" ios={archiveOutline} md={library} />
            <IonLabel>Manual</IonLabel>
          </IonItem>
          <IonItem className={location.pathname === "/login" ? 'selected' : ''} routerLink={"/login"} routerDirection="none" lines="none" disabled={!activarbotonlogin} detail={false}>
            <IonIcon slot="start" ios={archiveOutline} md={enterOutline} />
            <IonLabel>Login</IonLabel>
          </IonItem>
        </IonList>
      </IonContent>
    </IonMenu>

  );
};

export default Menu;
