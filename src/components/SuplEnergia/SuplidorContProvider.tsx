import React, { useEffect, useState } from 'react';
import suplidorContext, { suplidora, suplidoraContextModel } from './SuplodorCont';
import fb from '../BDconfig/firebaseConfig'

const SuplidorasContextProvider: React.FC = (props) => {
    const [posts, getPosts] = useState(false)
    async function listasuplidora() {
        const docref = await fb.collection(fb.firestore, `ListProvedor/Pais/LIstSuplidora`);
        const docucifrada = await fb.getDocs(docref)
        const inforfinal = docucifrada.docs.forEach(doc => {
            const { Precio, Suplidora } = doc.data();
            addsuplidoras(doc.id, Suplidora, Precio)
        });
    }
    useEffect(() => {
        if (posts === false) {
            listasuplidora();
            getPosts(true);

        }
    }, []);


    const [suplidoras, setsuplidoras] = useState<suplidora[]>([]);

    const addsuplidoras = (id: string, suplidora: string, precio: string) => {
        const addsuplidoras: suplidora = {
            id,
            suplidora,
            precio

        };

        setsuplidoras(currsuplidora => {
            return [...currsuplidora, addsuplidoras]
        })
    };
    const suplidorasContext: suplidoraContextModel = {
        suplidoras,
        addsuplidoras,
    };

    return (
        <suplidorContext.Provider value={suplidorasContext}>
            {props.children}
        </suplidorContext.Provider>
    );
};

export default SuplidorasContextProvider;