import React from 'react';

export interface suplidora {
    id: string;
    suplidora: string;
    precio: string;
}
export interface suplidoraContextModel {
    suplidoras: suplidora[];
    addsuplidoras: (id: string, suplidora: string, precio: string) => void
}
const suplidorContext = React.createContext<suplidoraContextModel>({
    suplidoras: [],
    addsuplidoras: () => { },

});

export default suplidorContext;