import React, { useState, useEffect } from 'react';
import img4 from '../asset/image/defaul.jpg'
import img2 from '../asset/image/limpiar.jpg'
import img3 from '../asset/image/cambiar.png'
import img from '../asset/image/apagar.jpg'
import ActivitiesContext, { Activity, ActivitiesContextModel, ActivityType } from './ActividadesConst';
import fb from '../BDconfig/firebaseConfig';

const ActivitiesContextProvider: React.FC = (props) => {
    const alert = (titulo: string, text: string, icon: string) => {
        fb.swal({
            title: titulo,
            text: text,
            icon: icon,
        });
    }
    const [username, setusername] = useState('');
    const [postslistaActivity, getPostslistaActivity] = useState(false)
    async function listsugerencia() {
        activities.splice(0);
        const docref = await fb.collection(fb.firestore, `ListProvedor/Pais/ListSugerencias`);
        const docucifrada = await fb.getDocs(docref)
        const inforfinal = docucifrada.docs.forEach(doc => {
            const { id, title, description, hour, activityType, imageUrl } = doc.data();
            const sugerencia = true;
            const isCompleted = false;
            addActivity(id, title, description, sugerencia, hour, activityType as ActivityType, imageUrl, isCompleted)
            
        });
       listActivity();
    }

    async function listActivity() {
        if (username) { } else { buscarusuario(); }
        const docref = await fb.collection(fb.firestore, `ListGeneral/${username}/ListActividad`);
        const docucifrada = await fb.getDocs(docref)
        const inforfinal = docucifrada.docs.forEach(doc => {
            const { id, title, description, sugerencia, hour, activityType, imageUrl, isCompleted } = doc.data();
            addActivity(id, title, description, sugerencia, hour, activityType, imageUrl, isCompleted)
        });
    }
    
    async function buscarusuario() {
        const user = fb.onAuthStateChanged(fb.auth, (usuariofirebase) => {
            if (usuariofirebase) {
                const username = {
                    uid: usuariofirebase.uid,
                    email: usuariofirebase.email,
                }
                setusername('' + username.uid)
            } return username;
        })
    }
    useEffect(() => {
        if (username) {
            if (postslistaActivity === false) {
                listsugerencia();
                getPostslistaActivity(true);
            }
        } else {
            buscarusuario();

        }

    });
    const addActivitynube = (title: string, description: string, sugerencia: boolean, hour: string, activityType: ActivityType, imageUrl: string) => {
        if (username) { } else { buscarusuario(); }
        addnube(title, description, sugerencia, hour, activityType)
    }

    /*cabis que ocurren en la nubes */
    async function Edinube(id: string) {
        try {
            const docref = await fb.doc(fb.firestore, `ListGeneral/${username}/ListActividad/${id}`)
            const process = await fb.updateDoc(docref, { isCompleted: "true" });
            listsugerencia();
        } catch (e) {
            alert('Error :(', 'Error al actualizar la list en la nube', 'warning');
        }
    }
    async function addnube(title: string, description: string, sugerencia: boolean, hour: string, activityType: ActivityType) {
        try {
            const id = Math.random().toString();
            const docref = await fb.doc(fb.firestore, `ListGeneral/${username}/ListActividad/${id}`)
            await fb.setDoc(docref, { id: id, title: title, description: description, sugerencia: sugerencia, isCompleted: false, hour: hour, activityType: activityType });
            listsugerencia();
        } catch (e) {
            alert('Error :(', 'Error al Agregar registro en la nube', 'warning');
        }
    }



    const [activities, setActivities] = useState<Activity[]>([]);
    const addActivity = (id: string, title: string, description: string, sugerencia: boolean, hour: string, activityType: ActivityType, imageUrl: string, isCompleted: boolean) => {
        if (imageUrl) { } else {
            switch (activityType) {
                case "apagar":
                    imageUrl = img
                    break;
                case "limpiar":
                    imageUrl = img2
                    break;
                case "cambiar":
                    imageUrl = img3
                    break;
                default:
                    imageUrl = img4
                    break;
            };
        }


        const addActivity: Activity = {

            id,
            sugerencia,
            title,
            description,
            hour,
            activityType,
            imageUrl,
            isCompleted,
        };

        setActivities(currActivities => {
            return [...currActivities, addActivity]
        })
    };


    const completeActivity = (activityId: string) => {
        Edinube(activityId);
    };

    const activitiesContext: ActivitiesContextModel = {
        activities,
        addActivitynube,
        completeActivity,
    };

    return (
        <ActivitiesContext.Provider value={activitiesContext}>
            {props.children}
        </ActivitiesContext.Provider>
    );
};

export default ActivitiesContextProvider;