import React from 'react';

export type ActivityType = 'apagar' | 'cambiar' | 'limpiar';

export interface Activity {
    id: string;
    sugerencia:  boolean;
    title: string;
    description: string;
    hour: string;
    activityType: ActivityType;
    imageUrl: string;
    isCompleted: boolean;
}

export interface ActivitiesContextModel {
    activities: Activity[];
    addActivitynube:(title: string, description: string,sugerencia: boolean, hour:string , activityType: ActivityType,imageUrl: string)=> void;
    completeActivity: (activityId: string) => void;
}

const ActivitiesContext = React.createContext<ActivitiesContextModel>({
    activities: [],
    addActivitynube: () => {},
    completeActivity: () => {}
});

export default ActivitiesContext;