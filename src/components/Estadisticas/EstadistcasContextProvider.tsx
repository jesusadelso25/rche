import React, { useState, useEffect } from 'react';
import fb from '../../components/BDconfig/firebaseConfig';

import equiposContext, { Equipo, equipoContextModel, NivelVoltaje } from './EstadisticaConst';

const EquiposCasaContextProvider: React.FC = (props) => {
    const mostraralerta = (title: string, text: string, icon: string, timer: number) => {
        fb.swal({
            title: title,
            text: text,
            icon: icon,
            timer: timer,
        });
    }
    const [username, setusername] = useState('');
    const [postslistacasa, getPostslistacasa] = useState(false)
    async function listacasa() {
        if (username) { } else { buscarusuario(); }
        equipos.splice(0);
        const docref = await fb.collection(fb.firestore, `ListGeneral/${username}/Listcasa`);
        const docucifrada = await fb.getDocs(docref)
        const inforfinal = docucifrada.docs.forEach(doc => {
            const { id, Equipo, Marca, hour, diasUso, SemanaUso, totalGasto, nivelvoltaje, imageUrl, voltaje, mes, ano } = doc.data();
            addequipos(id, Equipo, Marca, hour, diasUso, SemanaUso, totalGasto, nivelvoltaje as NivelVoltaje, voltaje, imageUrl, mes, ano)
        });
    }
    async function buscarusuario() {
        const user = fb.onAuthStateChanged(fb.auth, (usuariofirebase) => {
            if (usuariofirebase) {
                const username = {
                    uid: usuariofirebase.uid,
                    email: usuariofirebase.email,
                }
                setusername('' + username.uid)
            } return username;
        })
    }
    useEffect(() => {
        if (username) {
            if (postslistacasa === false) {
                listacasa();
                getPostslistacasa(true);
            }
        } else {
            buscarusuario();
        }
    });

    async function addnube(Equipo: string, Marca: string, hour: string, diasUso: string, SemanaUso: string, totalGasto: string, nivelvoltaje: NivelVoltaje, voltaje: string, imageUrl: string, mes: string, ano: string) {
        try {
            const id = Math.random().toString();
            const docref = await fb.doc(fb.firestore, `ListGeneral/${username}/Listcasa/${id}`)
            await fb.setDoc(docref, { id: id, Equipo: Equipo, Marca: Marca, hour: hour, diasUso: diasUso, SemanaUso: SemanaUso, totalGasto: totalGasto, nivelvoltaje: nivelvoltaje, voltaje: voltaje, imageUrl: imageUrl, mes: mes, ano: ano });
            return listacasa();
        } catch (e) {
            mostraralerta('Avertecia', 'Error en el servidor. !compruebe su conexión de ser necesario!', 'error', 40000);
        }
    }

    async function elimNube(Id: string) {
        try {
            const docref = await fb.doc(fb.firestore, `ListGeneral/${username}/Listcasa/${Id}`)
            const process = await fb.deleteDoc(docref);
            listacasa();
            return process;
        } catch (e) {
            mostraralerta('Avertecia', 'Error en el servidor. !compruebe su conexión de ser necesario!', 'error', 40000);
        }
    }


    const [equipos, setEquipos] = useState<Equipo[]>([]);
    const addaddequiposnube = (Equipo: string, Marca: string, hour: string, diasUso: string, SemanaUso: string, totalGasto: string, nivelvoltaje: NivelVoltaje, voltaje: string, imageUrl: string, mes: string, ano: string) => {
        if (username) { } else { buscarusuario(); }
        addnube(Equipo, Marca, hour, diasUso, SemanaUso, totalGasto, nivelvoltaje, voltaje, imageUrl, mes, ano);
    }
    const elimeqiponube = (id: string) => {
        if (username) { } else { buscarusuario(); }
        elimNube(id);
    }
    const addequipos = (id: string, Equipo: string, Marca: string, hour: string, diasUso: string, SemanaUso: string, totalGasto: string, nivelvoltaje: NivelVoltaje, voltaje: string, imageUrl: string, mes: string, ano: string) => {
        const addequipos: Equipo = {
            id,
            Equipo,
            Marca,
            hour,
            diasUso,
            SemanaUso,
            totalGasto,
            nivelvoltaje,
            imageUrl,
            voltaje,
            mes,
            ano
        };

        setEquipos(currequipos => {
            return [...currequipos, addequipos]
        })
    };


    const EquiposContext: equipoContextModel = {
        equipos,
        addaddequiposnube,
        elimeqiponube,
    };

    return (
        <equiposContext.Provider value={EquiposContext}>
            {props.children}
        </equiposContext.Provider>
    );
};

export default EquiposCasaContextProvider;