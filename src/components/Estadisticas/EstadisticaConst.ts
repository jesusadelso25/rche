import React from 'react';

export type NivelVoltaje = 'Alto' | 'Medio' | 'Bajo';
export interface Equipo {
    id: string;
    Equipo:  string;
    Marca: string;
    hour: string;
    diasUso: string;
    SemanaUso: string;
    totalGasto:string;
    nivelvoltaje: NivelVoltaje;
    imageUrl: string;
    voltaje: string;
    mes: string;
    ano: string;
}

export interface equipoContextModel {
    equipos: Equipo[];
    addaddequiposnube: (Equipo: string, Marca: string,hour: string,diasUso: string,SemanaUso: string,totalGasto:string,nivelvoltaje: NivelVoltaje,voltaje:string, imageUrl: string,mes: string,ano: string) => void;
    elimeqiponube:(ID: string)=> void;
}

const equiposContext = React.createContext<equipoContextModel>({
    equipos: [],
    addaddequiposnube: () => {},
    elimeqiponube:() => {},
});

export default equiposContext;