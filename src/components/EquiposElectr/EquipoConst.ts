import React from 'react';

export type NivelVoltaje = 'Alto' | 'Medio' | 'Bajo';

export interface Equipo {
    id: string;
    Equipo:  string;
    Marca: string;
    hour: string;
    nivelvoltaje: NivelVoltaje;
    imageUrl: string;
    voltaje: string;
}

export interface equipoContextModel {
    equipos: Equipo[];
    addequipos: (Equipo: string, Marca: string,hour: string,nivelvoltaje: NivelVoltaje,voltaje:string, imageUrl: string ) => void;
}

const equiposContext = React.createContext<equipoContextModel>({
    equipos: [],
    addequipos: () => {},
});

export default equiposContext;