import React, { useState, useEffect } from 'react';
import equiposContext, { Equipo, equipoContextModel, NivelVoltaje } from './EquipoConst';
import fb from '../../components/BDconfig/firebaseConfig'

const EquiposContextProvider: React.FC = (props) => {
    const [posts, getPosts] = useState(false)
    async function listasuplidora() {
            const docref = await fb.collection(fb.firestore, `ListProvedor/Pais/ListEquipos`);
            const docucifrada = await fb.getDocs(docref)
            const inforfinal = docucifrada.docs.forEach(doc => {
                const { Equipo, Marca, hour, nivelvoltaje, imageUrl, voltaje } = doc.data();
                addequipos(Equipo, Marca, hour, nivelvoltaje as NivelVoltaje, voltaje, imageUrl)
            });


    }
    useEffect(() => {
        if (posts === false) {
            listasuplidora();
            getPosts(true);

        }
    }, []);

    const [equipos, setEquipos] = useState<Equipo[]>([]);
    const addequipos = (Equipo: string, Marca: string, hour: string, nivelvoltaje: NivelVoltaje, voltaje: string, imageUrl: string) => {
        const addequipos: Equipo = {
            id: Math.random().toString(),
            Equipo,
            Marca,
            hour,
            nivelvoltaje,
            imageUrl,
            voltaje,
        };

        setEquipos(currequipos => {
            return [...currequipos, addequipos]
        })
    };


    const EquiposContext: equipoContextModel = {
        equipos,
        addequipos
    };

    return (
        <equiposContext.Provider value={EquiposContext}>
            {props.children}
        </equiposContext.Provider>
    );
};

export default EquiposContextProvider;