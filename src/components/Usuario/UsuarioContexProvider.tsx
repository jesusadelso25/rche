import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import UsersContext, { UserInfo, UserContextModel } from './UsuarioCont';
/*Importar user*/
import fb from '../BDconfig/firebaseConfig';

const UsersContextProvider: React.FC = (props) => {
    const direct = useHistory(); 
    const [Validaruser, SetValidaruser] = useState(false);

    const mostraralerta = (title: string, text: string, icon: string, timer: number) => {
        fb.swal({
            title: title,
            text: text,
            icon: icon,
            timer: timer,
        });
    }

    async function UserInfor(id: string, correoc: string) {
        const docref = await fb.collection(fb.firestore, `Usuario`);
        const docucifrada = await fb.getDocs(docref)
        const inforfinal = await docucifrada.docs.forEach(doc => {
            const { correo, suplidora } = doc.data();
            {
                if (correoc.toUpperCase() === correo.toUpperCase()) { addUseInfo(id, suplidora, correo); }
            }
        });
        SetValidaruser(true);


    }
    async function Bucarinfor() {
        const user = fb.onAuthStateChanged(fb.auth, (usuariofirebase) => {
            if (usuariofirebase) {
                const username = {
                    uid: usuariofirebase.uid,
                    email: usuariofirebase.email,
                }
                UserInfor("" + username.uid, "" + username.email)
            }
        })
    }
    /* Function que estrar las informaciones del  usuario  neserarias para la app*/
    const [UserInfo, setEUserInfo] = useState<UserInfo[]>([]);
    const addUseInfo = (id: string, idSuplidora: string, Email: string) => {
        const addequipos: UserInfo = {
            id,
            idSuplidora,
            Email,
        };

        setEUserInfo(currequipos => {
            return [...currequipos, addequipos]
        })
    };
    /* Function encargar de Validar el usuario */
    async function Validarusuario(Correo: string, Password: string) {
        try {
            await fb.signInWithEmailAndPassword(fb.auth, Correo, Password);
            Bucarinfor();
            direct.push("/Inicio");
        } catch (err) {
            mostraralerta('Avertecia', 'Error de password o username. !compruebe su conexión de ser necesario!', 'error', 40000);
        }
    }
    /* Function encargar de regitrar el usuario */
    async function Crearusuario(Correo: string, Password: string, idSuplidora: string) {
        try {
            const iduser = await fb.createUserWithEmailAndPassword(fb.auth, Correo, Password).then((usuariofaribase) => { return usuariofaribase })
            const docref = await fb.doc(fb.firestore, `Usuario/${iduser.user.uid}`)
            await fb.setDoc(docref, { correo: Correo, suplidora: idSuplidora })
            mostraralerta('Completado', 'guardado correctamente', 'success', 500000)
            Validarusuario(Correo, Password);
        } catch (err) {
            mostraralerta('ERROR', Correo + '..Este usuario ya fue agregado por alguien mas o Le faltan mas digisto a la contraseña', 'error', 2000)
        }
    }
    /* Function encargar de Actualizar el usuario */
    async function Updateusuario(Correo: string, Password: string, idSuplidora: string) {
        try {
            const iduser = await fb.createUserWithEmailAndPassword(fb.auth, Correo, Password).then((usuariofaribase) => { return usuariofaribase })
            const docref = await fb.doc(fb.firestore, `Usuario/${iduser.user.uid}`)
            await fb.setDoc(docref, { id: iduser.user.uid, correo: Correo, suplidora: idSuplidora })
            mostraralerta('Completado', 'guardado correctamente', 'success', 500000)
            Validarusuario(Correo, Password);
        } catch (err) {
            mostraralerta('ERROR', Correo + '..Este usuario ya fue agregado por alguien mas', 'error', 2000)
        }
    }
     /* traferencia de funcion y arreglo del usuario*/
    const UserContext: UserContextModel = {
        UserInfo,
        Validarusuario,
        Crearusuario,
        Updateusuario,
    };

    return (
        < UsersContext.Provider value={UserContext}>
            {props.children}
        </ UsersContext.Provider>
    );
};

export default UsersContextProvider;