import React from 'react';

export interface UserInfo {
    id: string;
    idSuplidora: string;
    Email: string;
}
export interface UseRef {
    Correo: string;
    Cotrasena: string;
}

export interface UserContextModel {
    UserInfo: UserInfo[];
    Validarusuario: (Correo: string,Cotrasena: string) => void;
    Crearusuario: (Correo: string,Cotrasena: string, idSuplidora: string) => void;
    Updateusuario: (Correo: string,Cotrasena: string, idSuplidora: string) => void;
}

const UserContext = React.createContext<UserContextModel>({
    UserInfo: [],
    Validarusuario: () => {},
    Crearusuario:() => {},
   Updateusuario:() => {},
});

export default UserContext;