import swal from 'sweetalert';
import{getAuth,signOut, onAuthStateChanged,signInWithEmailAndPassword,createUserWithEmailAndPassword}from "firebase/auth"
import {initializeApp} from "firebase/app";
import {getFirestore,doc,setDoc ,collection, getDocs,deleteDoc,updateDoc } from 'firebase/firestore'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBdvJTdw1T71B3v2Pov2ibn2T0L49RiyPQ",
  authDomain: "remotecontrolhomeenergy.firebaseapp.com",
  databaseURL: "https://remotecontrolhomeenergy-default-rtdb.firebaseio.com",
  projectId: "remotecontrolhomeenergy",
  storageBucket: "remotecontrolhomeenergy.appspot.com",
  messagingSenderId: "43359367095",
  appId: "1:43359367095:web:7ad9fc11debc65d69c8232"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const auth=getAuth(app);
export const firestore = getFirestore(app)

export default{
  app, auth,swal, 
  signOut,onAuthStateChanged,signInWithEmailAndPassword,createUserWithEmailAndPassword,updateDoc,
  firestore,doc,setDoc ,collection, getDocs,deleteDoc,
}